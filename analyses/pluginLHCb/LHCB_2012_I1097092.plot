BEGIN PLOT /LHCB_2012_I1097092/d01-x01-y01
Title=$\pi^+\pi^+\pi^-$ mass distribution in $B_C^+\to J/\psi \pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /LHCB_2012_I1097092/d02-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $B_C^+\to J/\psi \pi^+\pi^+\pi^-$
XLabel=$m_{\pi^+\pi^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT

BEGIN PLOT /LHCB_2012_I1097092/d03-x01-y01
Title=$\cos\theta_{B_c\mu}$ distribution in $B_C^+\to J/\psi(\mu^+\mu^-) \pi^+$
XLabel=$\cos\theta_{B_c\mu}$
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}\cos\theta$ 
LogY=0
END PLOT
BEGIN PLOT /LHCB_2012_I1097092/d03-x01-y02
Title=$\cos\theta_{B_c\mu}$ distribution in $B_C^+\to J/\psi(\mu^+\mu^-) \pi^+\pi^+\pi^-$
XLabel=$\cos\theta_{B_c\mu}$
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}\cos\theta$ 
LogY=0
END PLOT
