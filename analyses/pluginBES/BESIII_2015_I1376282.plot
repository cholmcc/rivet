BEGIN PLOT /BESIII_2015_I1376282/d01-x01-y01
Title=$K^0_SK^0_S\eta$ mass distribution in $J/\psi\to\gamma K^0_SK^0_S\eta$
XLabel=$m_{K^0_SK^0_S\eta}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S\eta}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1376282/d01-x01-y02
Title=$K^0_SK^0_S$ mass distribution in $J/\psi\to\gamma K^0_SK^0_S\eta$
XLabel=$m_{K^0_SK^0_S}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_SK^0_S}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1376282/d01-x01-y03
Title=$K^0_S\eta$ mass distribution in $J/\psi\to\gamma K^0_SK^0_S\eta$
XLabel=$m_{K^0_S\eta}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^0_S\eta}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1376282/d01-x01-y04
Title=$\cos\theta_\gamma$ distribution in $J/\psi\to\gamma K^0_SK^0_S\eta$
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1376282/d01-x01-y05
Title=$\cos\theta_\eta$ distribution in $J/\psi\to\gamma K^0_SK^0_S\eta$
XLabel=$\cos\theta_\eta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\eta$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2015_I1376282/d01-x01-y06
Title=$\cos\theta_{K^0_S}$ distribution in $J/\psi\to\gamma K^0_SK^0_S\eta$
XLabel=$\cos\theta_{K^0_S}$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_{K^0_S}$
LogY=0
END PLOT
