BEGIN PLOT /BESIII_2020_I1818254/d01-x01-y01
Title=$\bar{p}K^+$ mass distribution in $\chi_{c0}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m_{\bar{p}K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}K^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1818254/d01-x01-y02
Title=$\Sigma^0K^+$ mass distribution in $\chi_{c0}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m_{\Sigma^0K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^0K^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1818254/d01-x01-y03
Title=$\Sigma^0\bar{p}$ mass distribution in $\chi_{c0}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m_{\Sigma^0\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^0\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1818254/dalitz_1
Title=Dalitz plot for $\chi_{c0}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m^2_{\Sigma^0K^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{p}K^+}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\Sigma^0K^+}/{\rm d}m^2_{\bar{p}K^+}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2020_I1818254/d02-x01-y01
Title=$\bar{p}K^+$ mass distribution in $\chi_{c1}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m_{\bar{p}K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}K^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1818254/d02-x01-y02
Title=$\Sigma^0K^+$ mass distribution in $\chi_{c1}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m_{\Sigma^0K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^0K^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1818254/d02-x01-y03
Title=$\Sigma^0\bar{p}$ mass distribution in $\chi_{c1}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m_{\Sigma^0\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^0\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1818254/dalitz_2
Title=Dalitz plot for $\chi_{c1}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m^2_{\Sigma^0K^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{p}K^+}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\Sigma^0K^+}/{\rm d}m^2_{\bar{p}K^+}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT

BEGIN PLOT /BESIII_2020_I1818254/d03-x01-y01
Title=$\bar{p}K^+$ mass distribution in $\chi_{c2}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m_{\bar{p}K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar{p}K^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1818254/d03-x01-y02
Title=$\Sigma^0K^+$ mass distribution in $\chi_{c2}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m_{\Sigma^0K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^0K^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1818254/d03-x01-y03
Title=$\Sigma^0\bar{p}$ mass distribution in $\chi_{c2}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m_{\Sigma^0\bar{p}}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Sigma^0\bar{p}}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2020_I1818254/dalitz_3
Title=Dalitz plot for $\chi_{c2}\to \Sigma^0\bar{p}K^+$+c.c.
XLabel=$m^2_{\Sigma^0K^+}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\bar{p}K^+}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\Sigma^0K^+}/{\rm d}m^2_{\bar{p}K^+}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
