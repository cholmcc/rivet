Name: BESIII_2018_I1662665
Year: 2018
Summary: $\pi^0\eta$ mass distribution in  $D^0\to\pi^0\eta\eta$
Experiment: BESIII
Collider: BEPC
InspireID: 1662665
Status: VALIDATED NOHEPDATA
Reentrant: true
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Lett.B 781 (2018) 368-375
RunInfo: Any process producing D0 mesons
Description:
  'Measurement of the $\pi^0\eta$ mass distribution in  $D^0\to\pi^0\eta\eta$ by BES. The data were read from the plots in the paper. It is also not clear that any resolution effects have been unfolded.'
ValidationInfo:
  'Herwig 7 events using model in the paper'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2018hui
BibTeX: '@article{BESIII:2018hui,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Measurement of singly Cabibbo-suppressed decays $D^{0}\to\pi^{0}\pi^{0}\pi^{0}$, $\pi^{0}\pi^{0}\eta$, $\pi^{0}\eta\eta$ and $\eta\eta\eta$}",
    eprint = "1803.05769",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1016/j.physletb.2018.04.017",
    journal = "Phys. Lett. B",
    volume = "781",
    pages = "368--375",
    year = "2018"
}
'
