BEGIN PLOT /BESIII_2012_I1079921/d01-x01-y01
Title=$p\bar{p}$ mass distribution in $J/\psi\to\gamma p\bar{p}$
XLabel=$m_{p\bar{p}}-2m_p$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{p\bar{p}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1079921/d01-x01-y02
Title=$\cos\theta_\gamma$ distribution in $J/\psi\to\gamma p\bar{p}$
XLabel=$\cos\theta_\gamma$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_\gamma$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1079921/d01-x01-y03
Title=$\cos\theta_p$ distribution in $J/\psi\to\gamma p\bar{p}$
XLabel=$\cos\theta_p$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta_p$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1079921/d01-x01-y04
Title=$\phi_p$ distribution in $J/\psi\to\gamma p\bar{p}$
XLabel=$\phi_p$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi_p$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2012_I1079921/d02-x01-y01
Title=$p\bar{p}$ mass distribution in $\psi(2S)\to\gamma p\bar{p}$
XLabel=$m_{p\bar{p}}-2m_p$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{p\bar{p}}$ [$\text{GeV}^{-1}$]
LogY=0
END PLOT
