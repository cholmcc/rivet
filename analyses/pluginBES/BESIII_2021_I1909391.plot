BEGIN PLOT /BESIII_2021_I1909391/d01-x01-y01
Title=$\pi^+\pi^-$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1909391/d01-x01-y02
Title=$\pi^+\pi^+$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^+}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^+}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1909391/d01-x01-y03
Title=Lower $\pi^+\pi^-$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1909391/d01-x01-y04
Title=Higher $\pi^+\pi^-$ mass distribution in $D_s^+\to \pi^+\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2021_I1909391/dalitz
Title=Dalitz plot for $D_s^+\to \pi^+\pi^+\pi^-$
XLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\pi^+\pi^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{\pi^+\pi^-}/{\rm d}m^2_{\pi^+\pi^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
