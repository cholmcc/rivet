BEGIN PLOT /BESIII_2021_I1868813/d01-x01-y01
Title=$\sigma(e^+e^-\to K^0_{S}+X)$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(e^+e^-\to K^0_{S}+X))$
ConnectGaps=1
END PLOT
