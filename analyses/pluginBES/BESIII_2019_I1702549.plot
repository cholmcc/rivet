BEGIN PLOT /BESIII_2019_I1702549
LogY=0
END PLOT
BEGIN PLOT /BESIII_2019_I1702549/d01-x01-y01
Title=$D_s^+\to K^0 e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{ns}^{-1}\mathrm{GeV}^{-2}$]
END PLOT
BEGIN PLOT /BESIII_2019_I1702549/d01-x01-y03
Title=$K^+\pi^-$ mass in $D_s^+\to K^+\pi^- e^+\nu_e$
XLabel=$m_{K^+\pi^-}$~[GeV]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}m_{K^+\pi^-}$ [$\mathrm{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2019_I1702549/d01-x01-y04
Title=$e^+\nu_e$ mass squared in $D_s^+\to K^+\pi^- e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}q^2$ [$\mathrm{GeV}^{-2}$]
END PLOT

BEGIN PLOT /BESIII_2019_I1702549/d01-x01-y05
Title=$\cos\theta_e$ in $D_s^+\to K^+\pi^- e^+\nu_e$
XLabel=$\cos\theta_e$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_e$ 
END PLOT

BEGIN PLOT /BESIII_2019_I1702549/d01-x01-y06
Title=$\cos\theta_K$ in $D_s^+\to K^+\pi^- e^+\nu_e$
XLabel=$\cos\theta_K$
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_K$ 
END PLOT

BEGIN PLOT /BESIII_2019_I1702549/d01-x01-y07
Title=$\chi$ in $D_s^+\to K^+\pi^- e^+\nu_e$
XLabel=$\chi$ [rad]
YLabel=$1/\Gamma\mathrm{d}\Gamma/\mathrm{d}\cos\theta_K$ [$\mathrm{rad}^{-1}$]
END PLOT