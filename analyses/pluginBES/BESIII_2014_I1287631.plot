BEGIN PLOT /BESIII_2014_I1287631
XLabel=$m_{e^+e^-}$ [GeV]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{e^+e^-}$ [$\text{GeV}^{-1}$]
END PLOT
BEGIN PLOT /BESIII_2014_I1287631/d01-x01-y01
Title=$e^+e^-$ mass distribution in $J/\psi\to\eta^\prime e^+e^-$ (using $\eta^\prime\to\gamma\pi^+\pi^-$)
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1287631/d01-x01-y02
Title=$e^+e^-$ mass distribution in $J/\psi\to\eta^\prime e^+e^-$ (using $\eta^\prime\to\eta\pi^+\pi^-$)
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1287631/d01-x01-y03
Title=$e^+e^-$ mass distribution in $J/\psi\to\eta e^+e^-$ (using $\eta\to\pi^+\pi^-\pi^0$)
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1287631/d01-x01-y04
Title=$e^+e^-$ mass distribution in $J/\psi\to\eta e^+e^-$ (using $\eta\to\gamma\gamma$)
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1287631/d01-x01-y05
Title=$e^+e^-$ mass distribution in $J/\psi\to\pi^0 e^+e^-$ 
LogY=0
END PLOT
BEGIN PLOT /BESIII_2014_I1287631/d02-x01-y01
Title=$J/\psi\to\eta^\prime e^+e^-$ form factor
YLabel=$\left|F_{J/\psi\to\eta^\prime}\left(m^2_{e^+e^-}\right)\right|^2$
LogY=0
END PLOT
