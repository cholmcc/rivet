BEGIN PLOT /BESIII_2022_I2105430/d01-x01-y01
Title=$X$ distribution for $\eta^\prime\to\eta\pi^0\pi^0$
XLabel=$X$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}X$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2105430/d02-x01-y01
Title=$Y$ distribution for $\eta^\prime\to\eta\pi^0\pi^0$
XLabel=$Y$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}Y$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2105430/d03-x01-y01
Title=Ratio of $m^2_{\eta\pi^0}$ distribution to phase-space for $\eta^\prime\to\eta\pi^0\pi^0$
XLabel=$m^2_{\eta\pi^0}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\eta\pi^0}/\text{PHSP}$
LogY=0
END PLOT
BEGIN PLOT /BESIII_2022_I2105430/d04-x01-y01
Title=Ratio of $m^2_{\pi^0\pi^0}$ distribution to phase-space for $\eta^\prime\to\eta\pi^0\pi^0$
XLabel=$m^2_{\pi^0\pi^0}$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m^2_{\pi^0\pi^0}/\text{PHSP}$
LogY=0
END PLOT
