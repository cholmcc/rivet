BEGIN PLOT /BESIII_2013_I1261765/d01-x01-y01
Title=$\Lambda^0\pi^\mp$ mass distribution in $\psi(2S)\to \Lambda^0\bar\Sigma^\pm\pi^\mp+$+c.c.
XLabel=$m_{\Lambda^0\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda^0\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1261765/d01-x01-y02
Title=$\bar\Sigma^\pm\pi^\mp$ mass distribution in $\psi(2S)\to \Lambda^0\bar\Sigma^\pm\pi^\mp+$+c.c.
XLabel=$m_{\bar\Sigma^\pm\pi^\mp}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\bar\Sigma^\pm\pi^\mp}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BESIII_2013_I1261765/d01-x01-y03
Title=$\Lambda^0\bar\Sigma^\pm$ mass distribution in $\psi(2S)\to \Lambda^0\bar\Sigma^\pm\pi^\mp+$+c.c.
XLabel=$m_{\Lambda^0\bar\Sigma^\pm}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda^0\bar\Sigma^\pm}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
