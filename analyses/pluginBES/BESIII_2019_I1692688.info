Name: BESIII_2019_I1692688
Year: 2019
Summary: $e^+e^-$ mass distribution in $J/\psi\to\eta^\prime e^+e^-$
Experiment: BESIII
Collider: BEPC
InspireID: 1692688
Status: VALIDATED NOHEPDATA
Reentrant: false
Authors:
 - Peter Richardson <peter.richardson@durham.ac.uk>
References:
 - Phys.Rev.D 99 (2019) 1, 012013
RunInfo: Any process producing J/psi
Description:
  '$e^+e^-$ mass distribution in $J/\psi\to\eta^\prime e^+e^-$. Data read from plots in paper which are not efficiency corrected.'
ValidationInfo:
  'Herwig 7 events'
#ReleaseTests:
# - $A my-hepmc-prefix :MODE=some_rivet_flag
Keywords: []
BibKey: BESIII:2018aao
BibTeX: '@article{BESIII:2018aao,
    author = "Ablikim, Medina and others",
    collaboration = "BESIII",
    title = "{Measurement of $\mathcal{B}(J/\psi \to \eta^\prime e^+ e^-)$ and search for a dark photon}",
    eprint = "1809.00635",
    archivePrefix = "arXiv",
    primaryClass = "hep-ex",
    doi = "10.1103/PhysRevD.99.012013",
    journal = "Phys. Rev. D",
    volume = "99",
    number = "1",
    pages = "012013",
    year = "2019"
}
'
