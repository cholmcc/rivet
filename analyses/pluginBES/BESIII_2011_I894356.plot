BEGIN PLOT /BESIII_2011_I894356/d01-x01-y
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT

BEGIN PLOT /BESIII_2011_I894356/d01-x01-y01
Title=Helicity angle in $\chi_{c1}\to \gamma\phi(\to K^+K^-)$
END PLOT
BEGIN PLOT /BESIII_2011_I894356/d01-x01-y02
Title=Helicity angle in $\chi_{c1}\to \gamma\rho^0(\to  \pi^+\pi^-)$
END PLOT
BEGIN PLOT /BESIII_2011_I894356/d01-x01-y03
Title=Helicity angle in $\chi_{c1}\to \gamma\omega(\to \pi^+\pi^-\pi^0)$
END PLOT
