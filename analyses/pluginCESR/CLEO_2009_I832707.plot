BEGIN PLOT /CLEO_2009_I832707
LogY=0
END PLOT
BEGIN PLOT /CLEO_2009_I832707/d01-x01-y01
Title=$|\cos\theta_3|$ for $\ell^+$ in $\psi(2S)\to\gamma_1\chi_{c1}$, $\chi_{c1}\to\gamma_2J/\psi$, $J\psi\to\ell^+\ell^-$
XLabel=$|\cos\theta_3|$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}|\cos\theta_3|$
END PLOT
BEGIN PLOT /CLEO_2009_I832707/d01-x01-y02
Title=$|\cos\theta_3|$ for $\ell^+$ in $\psi(2S)\to\gamma_1\chi_{c2}$, $\chi_{c2}\to\gamma_2J/\psi$, $J\psi\to\ell^+\ell^-$
XLabel=$|\cos\theta_3|$
YLabel=$1/\sigma\mathrm{d}\sigma/\mathrm{d}|\cos\theta_3|$
END PLOT
