BEGIN PLOT /BABAR_2011_I920989/d01
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BABAR_2011_I920989/d01-x01-y01
Title=$D^*$ helicity angle in $\bar{B}^0\toD^{*0}\omega$ (using $D^0\toK^-\pi^+$)
END PLOT
BEGIN PLOT /BABAR_2011_I920989/d01-x01-y02
Title=$\omega$ helicity angle in $\bar{B}^0\toD^{*0}\omega$ (using $D^0\toK^-\pi^+$)
END PLOT
BEGIN PLOT /BABAR_2011_I920989/d01-x02-y01
Title=$D^*$ helicity angle in $\bar{B}^0\toD^{*0}\omega$ (using $D^0\toK^-\pi^+\pi^0$)
END PLOT
BEGIN PLOT /BABAR_2011_I920989/d01-x02-y02
Title=$\omega$ helicity angle in $\bar{B}^0\toD^{*0}\omega$ (using $D^0\toK^-\pi^+\pi^0$)
END PLOT
BEGIN PLOT /BABAR_2011_I920989/d01-x03-y01
Title=$D^*$ helicity angle in $\bar{B}^0\toD^{*0}\omega$ (using $D^0\toK^-\pi^+\pi^-\pi^+$)
END PLOT
BEGIN PLOT /BABAR_2011_I920989/d01-x03-y02
Title=$\omega$ helicity angle in $\bar{B}^0\toD^{*0}\omega$ (using $D^0\toK^-\pi^+\pi^-\pi^+$)
END PLOT
BEGIN PLOT /BABAR_2011_I920989/d01-x04-y01
Title=$D^*$ helicity angle in $\bar{B}^0\toD^{*0}\omega$ (using $D^0\toK^0_S\pi^+\pi^-$)
END PLOT
BEGIN PLOT /BABAR_2011_I920989/d01-x04-y02
Title=$\omega$ helicity angle in $\bar{B}^0\toD^{*0}\omega$ (using $D^0\toK^0_S\pi^+\pi^-$)
END PLOT
