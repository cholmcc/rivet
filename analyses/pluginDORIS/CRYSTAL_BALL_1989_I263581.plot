BEGIN PLOT /CRYSTAL_BALL_1989_I263581/d01-x01-y01
Title=Inclusive electron spectrum in $B$ decays at the $\Upsilon(4S)$
XLabel=$E$ [GeV]
YLabel=$1/\Gamma_{\mathrm{total}}\mathrm{d}\Gamma/\mathrm{d}E$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
