# BEGIN PLOT /CMS_2017_I1634835/*
Title=CMS, 8 TeV, Z+charm jets
LegendAlign=r
LogX = 0
LogY = 0
RatioPlotYMin=0
RatioPlotYMax=3
# END PLOT
# BEGIN PLOT /CMS_2017_I1634835/d04-x01-y01
XLabel=p^{\text{Z}}_{\text{T}}$ [GeV]
YLabel=$d\sigma(Z+c)/dp^{\text{Z}}_{\text{T}}$ [pb/GeV]
# END PLOT
# BEGIN PLOT /CMS_2017_I1634835/d04-x01-y02
XLabel=p^{\text{Z}}_{\text{T}}$ [GeV]
YLabel=$d\sigma(Z+c)/dp^{\text{Z}}_{\text{T}} / d\sigma(Z+b)/dp^{\text{Z}}_{\text{T}}$
# END PLOT
# BEGIN PLOT /CMS_2017_I1634835/d05-x01-y01
XLabel=p^{\text{jet}}_{\text{T}}$ [GeV]
YLabel=$d\sigma(Z+c)/dp^{\text{jet}}_{\text{T}}$ [pb/GeV]
# END PLOT
# BEGIN PLOT /CMS_2017_I1634835/d05-x01-y02
XLabel=p^{\text{jet}}_{\text{T}}$ [GeV]
YLabel=$d\sigma(Z+c)/dp^{\text{jet}}_{\text{T}} / d\sigma(Z+b)/dp^{\text{jet}}_{\text{T}}$
# END PLOT
