BEGIN PLOT /BELLE_2018_I1641071/d01-x01-y01
Title=$\pi^+\pi^0$ mass distribution in $\Omega_c^0\to\Omega^-\pi^+\pi^0$
XLabel=$m_{\pi^+\pi^0}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\pi^+\pi^0}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2018_I1641071/d01-x01-y02
Title=$\Xi^-\pi^+$ mass distribution in $\Omega_c^0\to\Xi^- K^-\pi^+\pi^+$
XLabel=$m_{\Xi^-\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Xi^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2018_I1641071/d01-x01-y03
Title=$K^-\pi^+$ mass distribution in $\Omega_c^0\to\Xi^- K^-\pi^+\pi^+$
XLabel=$m_{K^-\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2018_I1641071/d01-x01-y04
Title=$K^-\pi^+$ mass distribution in $\Omega_c^0\to\Xi^0 K^-\pi^+$
XLabel=$m_{K^-\pi^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^-\pi^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
