BEGIN PLOT /BELLE_2022_I2163247/d01-x01-y01
Title=$B^0\to \pi^- e^+\nu_e$
XLabel=$q^2$~[GeV$^2$]
YLabel=$\text{d}\mathcal{B}/\text{d}q^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2163247/d01-x01-y02
Title=$B^0\to \pi^- \mu^+\nu_\mu$
XLabel=$q^2$~[GeV$^2$]
YLabel=$\text{d}\mathcal{B}/\text{d}q^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2022_I2163247/d01-x01-y03
Title=$B^0\to \pi^- \ell^+\nu_\ell$
XLabel=$q^2$~[GeV$^2$]
YLabel=$\text{d}\mathcal{B}/\text{d}q^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
