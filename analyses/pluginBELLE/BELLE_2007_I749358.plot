BEGIN PLOT /BELLE_2007_I749358
XLabel=$|\cos\theta|$
YLabel=$\mathrm{d}\sigma(\gamma\gamma\to \pi^+\pi^-)/\mathrm{d}|\cos\theta|$ [nb]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2007_I749358/d01-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$ [GeV]
YLabel=$\sigma(\gamma\gamma\to \pi^+\pi^-)$ [nb]
LogY=1
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d02-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.800<\sqrt{s}<0.805$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d03-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.805<\sqrt{s}<0.810$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d04-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.810<\sqrt{s}<0.815$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d05-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.815<\sqrt{s}<0.820$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d06-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.820<\sqrt{s}<0.825$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d07-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.825<\sqrt{s}<0.830$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d08-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.830<\sqrt{s}<0.835$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d09-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.835<\sqrt{s}<0.840$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d10-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.840<\sqrt{s}<0.845$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d11-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.845<\sqrt{s}<0.850$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d12-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.850<\sqrt{s}<0.855$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d13-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.855<\sqrt{s}<0.860$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d14-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.860<\sqrt{s}<0.865$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d15-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.865<\sqrt{s}<0.870$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d16-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.870<\sqrt{s}<0.875$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d17-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.875<\sqrt{s}<0.880$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d18-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.880<\sqrt{s}<0.885$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d19-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.885<\sqrt{s}<0.890$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d20-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.890<\sqrt{s}<0.895$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d21-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.895<\sqrt{s}<0.900$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d22-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.900<\sqrt{s}<0.905$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d23-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.905<\sqrt{s}<0.910$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d24-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.910<\sqrt{s}<0.915$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d25-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.915<\sqrt{s}<0.920$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d26-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.920<\sqrt{s}<0.925$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d27-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.925<\sqrt{s}<0.930$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d28-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.930<\sqrt{s}<0.935$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d29-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.935<\sqrt{s}<0.940$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d30-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.940<\sqrt{s}<0.945$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d31-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.945<\sqrt{s}<0.950$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d32-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.950<\sqrt{s}<0.955$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d33-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.955<\sqrt{s}<0.960$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d34-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.960<\sqrt{s}<0.965$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d35-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.965<\sqrt{s}<0.970$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d36-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.970<\sqrt{s}<0.975$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d37-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.975<\sqrt{s}<0.980$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d38-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.980<\sqrt{s}<0.985$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d39-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.985<\sqrt{s}<0.990$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d40-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.990<\sqrt{s}<0.995$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d41-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $0.995<\sqrt{s}<1.000$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d42-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.000<\sqrt{s}<1.005$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d43-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.005<\sqrt{s}<1.010$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d44-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.010<\sqrt{s}<1.015$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d45-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.015<\sqrt{s}<1.020$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d46-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.020<\sqrt{s}<1.025$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d47-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.025<\sqrt{s}<1.030$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d48-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.030<\sqrt{s}<1.035$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d49-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.035<\sqrt{s}<1.040$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d50-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.040<\sqrt{s}<1.045$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d51-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.045<\sqrt{s}<1.050$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d52-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.050<\sqrt{s}<1.055$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d53-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.055<\sqrt{s}<1.060$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d54-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.060<\sqrt{s}<1.065$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d55-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.065<\sqrt{s}<1.070$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d56-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.070<\sqrt{s}<1.075$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d57-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.075<\sqrt{s}<1.080$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d58-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.080<\sqrt{s}<1.085$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d59-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.085<\sqrt{s}<1.090$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d60-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.090<\sqrt{s}<1.095$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d61-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.095<\sqrt{s}<1.100$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d62-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.100<\sqrt{s}<1.105$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d63-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.105<\sqrt{s}<1.110$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d64-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.110<\sqrt{s}<1.115$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d65-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.115<\sqrt{s}<1.120$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d66-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.120<\sqrt{s}<1.125$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d67-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.125<\sqrt{s}<1.130$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d68-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.130<\sqrt{s}<1.135$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d69-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.135<\sqrt{s}<1.140$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d70-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.140<\sqrt{s}<1.145$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d71-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.145<\sqrt{s}<1.150$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d72-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.150<\sqrt{s}<1.155$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d73-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.155<\sqrt{s}<1.160$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d74-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.160<\sqrt{s}<1.165$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d75-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.165<\sqrt{s}<1.170$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d76-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.170<\sqrt{s}<1.175$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d77-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.175<\sqrt{s}<1.180$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d78-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.180<\sqrt{s}<1.185$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d79-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.185<\sqrt{s}<1.190$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d80-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.190<\sqrt{s}<1.195$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d81-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.195<\sqrt{s}<1.200$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d82-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.200<\sqrt{s}<1.205$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d83-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.205<\sqrt{s}<1.210$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d84-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.210<\sqrt{s}<1.215$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d85-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.215<\sqrt{s}<1.220$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d86-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.220<\sqrt{s}<1.225$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d87-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.225<\sqrt{s}<1.230$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d88-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.230<\sqrt{s}<1.235$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d89-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.235<\sqrt{s}<1.240$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d90-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.240<\sqrt{s}<1.245$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d91-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.245<\sqrt{s}<1.250$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d92-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.250<\sqrt{s}<1.255$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d93-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.255<\sqrt{s}<1.260$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d94-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.260<\sqrt{s}<1.265$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d95-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.265<\sqrt{s}<1.270$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d96-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.270<\sqrt{s}<1.275$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d97-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.275<\sqrt{s}<1.280$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d98-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.280<\sqrt{s}<1.285$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d99-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.285<\sqrt{s}<1.290$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d100-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.290<\sqrt{s}<1.295$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d101-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.295<\sqrt{s}<1.300$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d102-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.300<\sqrt{s}<1.305$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d103-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.305<\sqrt{s}<1.310$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d104-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.310<\sqrt{s}<1.315$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d105-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.315<\sqrt{s}<1.320$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d106-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.320<\sqrt{s}<1.325$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d107-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.325<\sqrt{s}<1.330$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d108-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.330<\sqrt{s}<1.335$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d109-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.335<\sqrt{s}<1.340$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d110-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.340<\sqrt{s}<1.345$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d111-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.345<\sqrt{s}<1.350$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d112-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.350<\sqrt{s}<1.355$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d113-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.355<\sqrt{s}<1.360$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d114-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.360<\sqrt{s}<1.365$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d115-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.365<\sqrt{s}<1.370$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d116-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.370<\sqrt{s}<1.375$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d117-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.375<\sqrt{s}<1.380$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d118-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.380<\sqrt{s}<1.385$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d119-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.385<\sqrt{s}<1.390$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d120-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.390<\sqrt{s}<1.395$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d121-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.395<\sqrt{s}<1.400$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d122-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.400<\sqrt{s}<1.405$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d123-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.405<\sqrt{s}<1.410$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d124-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.410<\sqrt{s}<1.415$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d125-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.415<\sqrt{s}<1.420$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d126-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.420<\sqrt{s}<1.425$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d127-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.425<\sqrt{s}<1.430$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d128-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.430<\sqrt{s}<1.435$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d129-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.435<\sqrt{s}<1.440$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d130-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.440<\sqrt{s}<1.445$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d131-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.445<\sqrt{s}<1.450$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d132-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.450<\sqrt{s}<1.455$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d133-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.455<\sqrt{s}<1.460$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d134-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.460<\sqrt{s}<1.465$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d135-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.465<\sqrt{s}<1.470$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d136-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.470<\sqrt{s}<1.475$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d137-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.475<\sqrt{s}<1.480$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d138-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.480<\sqrt{s}<1.485$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d139-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.485<\sqrt{s}<1.490$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d140-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.490<\sqrt{s}<1.495$ GeV
END PLOT
BEGIN PLOT /BELLE_2007_I749358/d141-x01-y01
Title=Cross section for $\gamma\gamma\to \pi^+\pi^-$ with $1.495<\sqrt{s}<1.500$ GeV
END PLOT

