BEGIN PLOT /BELLE_2014_I1312626/d01-x01-y01
Title=$K^-\pi^+$ mass distribution in $\bar{B}^0\to J/\psi K^-\pi^+$ ($m^2_{J/\psi\pi^+}<16\text{GeV}^2$)
XLabel=$m_{K^-\pi^+}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1312626/d01-x01-y02
Title=$K^-\pi^+$ mass distribution in $\bar{B}^0\to J/\psi K^-\pi^+$ ($16<m^2_{J/\psi\pi^+}<19\text{GeV}^2$)
XLabel=$m_{K^-\pi^+}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1312626/d01-x01-y03
Title=$K^-\pi^+$ mass distribution in $\bar{B}^0\to J/\psi K^-\pi^+$ ($m^2_{J/\psi\pi^+}>19\text{GeV}^2$)
XLabel=$m_{K^-\pi^+}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{K^-\pi^+}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1312626/d01-x01-y04
Title=$J/\psi\pi^+$ mass distribution in $\bar{B}^0\to J/\psi K^-\pi^+$ ($m^2_{K\pi^+}<1.2\text{GeV}^2$)
XLabel=$m_{J/\psi\pi^+}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi^+}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1312626/d01-x01-y05
Title=$J/\psi\pi^+$ mass distribution in $\bar{B}^0\to J/\psi K^-\pi^+$ ($1.2<m^2_{K\pi^+}<2.05\text{GeV}^2$)
XLabel=$m_{J/\psi\pi^+}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi^+}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1312626/d01-x01-y06
Title=$J/\psi\pi^+$ mass distribution in $\bar{B}^0\to J/\psi K^-\pi^+$ ($2.05<m^2_{K\pi^+}<3.2\text{GeV}^2$)
XLabel=$m_{J/\psi\pi^+}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi^+}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1312626/d01-x01-y07
Title=$J/\psi\pi^+$ mass distribution in $\bar{B}^0\to J/\psi K^-\pi^+$ ($m^2_{K\pi^+}>3.2\text{GeV}^2$)
XLabel=$m_{J/\psi\pi^+}^2$ [$\text{GeV}^2$]
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}m_{J/\psi\pi^+}^2$ [$\text{GeV}^{-2}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1312626/d02-x01-y01
Title=Helicity ngle in $\bar{B}^0\to J/\psi(\to\ell^+\ell^-) K^-\pi^+$
XLabel=$\cos\theta$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\cos\theta$
LogY=0
END PLOT
BEGIN PLOT /BELLE_2014_I1312626/d02-x01-y02
Title=Azimuthal angle in $\bar{B}^0\to J/\psi(\to\ell^+\ell^-) K^-\pi^+$
XLabel=$\phi$
YLabel=$1/\Gamma\text{d}\Gamma/\text{d}\phi$
LogY=0
END PLOT
