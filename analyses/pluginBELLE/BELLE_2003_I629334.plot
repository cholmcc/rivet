BEGIN PLOT /BELLE_2003_I629334
XLabel=$|\cos\theta|$
YLabel=$\mathrm{d}\sigma(\gamma\gamma\to K^+K^-)/\mathrm{d}|\cos\theta|$ [nb]
LogY=0
END PLOT

BEGIN PLOT /BELLE_2003_I629334/d01-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $|\cos\theta|<0.6$
XLabel=$\sqrt{s}$
YLabel=$\sigma(\gamma\gamma\to K^+K^-)$ [nb]
LogY=1
END PLOT


BEGIN PLOT /BELLE_2003_I629334/d02-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.40<\sqrt{s}<1.44$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d02-x01-y02
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.44<\sqrt{s}<1.48$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d02-x01-y03
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.48<\sqrt{s}<1.52$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d02-x01-y04
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.52<\sqrt{s}<1.56$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d03-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.56<\sqrt{s}<1.60$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d03-x01-y02
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.60<\sqrt{s}<1.64$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d03-x01-y03
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.64<\sqrt{s}<1.68$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d03-x01-y04
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.68<\sqrt{s}<1.72$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d04-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.72<\sqrt{s}<1.76$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d04-x01-y02
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.76<\sqrt{s}<1.80$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d04-x01-y03
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.80<\sqrt{s}<1.84$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d04-x01-y04
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.84<\sqrt{s}<1.88$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d05-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.88<\sqrt{s}<1.92$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d05-x01-y02
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.92<\sqrt{s}<1.96$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d05-x01-y03
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $1.96<\sqrt{s}<2.00$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d05-x01-y04
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $2.00<\sqrt{s}<2.04$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d06-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $2.04<\sqrt{s}<2.08$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d06-x01-y02
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $2.08<\sqrt{s}<2.12$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d06-x01-y03
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $2.12<\sqrt{s}<2.16$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d06-x01-y04
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $2.16<\sqrt{s}<2.20$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d07-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $2.20<\sqrt{s}<2.24$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d07-x01-y02
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $2.24<\sqrt{s}<2.28$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d07-x01-y03
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $2.28<\sqrt{s}<2.32$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d07-x01-y04
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $2.32<\sqrt{s}<2.36$ GeV
END PLOT
BEGIN PLOT /BELLE_2003_I629334/d08-x01-y01
Title=Cross section for $\gamma\gamma\to K^+K^-$ with $2.36<\sqrt{s}<2.40$ GeV
END PLOT
