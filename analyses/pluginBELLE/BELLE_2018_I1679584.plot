BEGIN PLOT /BELLE_2018_I1679584/d01-x01-y01
Title=$\Lambda_c^+K^0_S$ mass distribution in $\bar{B}^0\to \Lambda_c^+\bar{\Lambda}^-_c K^0_S$
XLabel=$m_{\Lambda_c^+K^0_S}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+K^0_S}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2018_I1679584/d02-x01-y01
Title=$\Lambda_c^+\bar \Lambda_c^-$ mass distribution in $\bar{B}^0\to \Lambda_c^+\bar{\Lambda}^-_c K^0_S$
XLabel=$m_{\Lambda_c^+\bar \Lambda_c^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Lambda_c^+\bar \Lambda_c^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
