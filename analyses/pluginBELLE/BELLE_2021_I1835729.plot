BEGIN PLOT /BELLE_2021_I1835729/d01-x01-y01
Title=$K^+K^-$ mass distribution in $\Xi_c^0\to K^+K^-\Xi^0$
XLabel=$m_{K^+K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{K^+K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1835729/d01-x01-y02
Title=$\Xi^0K^-$ mass distribution in $\Xi_c^0\to K^+K^-\Xi^0$
XLabel=$m_{\Xi^0K^-}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Xi^0K^-}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1835729/d01-x01-y03
Title=$\Xi^0K^+$ mass distribution in $\Xi_c^0\to K^+K^-\Xi^0$
XLabel=$m_{\Xi^0K^+}$ [$\mathrm{GeV}$]
YLabel=$1/\Gamma\mathrm{d} \Gamma/\mathrm{d}m_{\Xi^0K^+}$ [$\mathrm{GeV}^{-1}$]
LogY=0
END PLOT
BEGIN PLOT /BELLE_2021_I1835729/dalitz
Title=Dalitz plot for  $\Xi_c^0\to K^+K^-\Xi^0$
XLabel=$m^2_{K^+K^-}$ [$\mathrm{GeV}^2$]
YLabel=$m^2_{\Xi^0K^-}$ [$\mathrm{GeV}^2$]
ZLabel=$1/\Gamma{\rm d}^2 \Gamma/{\rm d}m^2_{K^+K^-}/{\rm d}m^2_{\Xi^0K^-}$ [$\rm{GeV}^{-4}$]
LogY=0
END PLOT
