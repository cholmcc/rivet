BEGIN PLOT /MARKII_1987_I247900/d01-x01-y01
Title=$\Omega^-$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}x_E$ [$\mathrm{nb} \mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /MARKII_1987_I247900/d02-x01-y01
Title=$\Omega^-$ cross section at 29 GeV
XLabel=$\sqrt{s}$
YLabel=$\sigma(\Omega^-)$
END PLOT
BEGIN PLOT /MARKII_1987_I247900/d02-x01-y02
Title=$\Omega^-$ rate at 29 GeV
XLabel=$\sqrt{s}$
YLabel=$N(\Omega^-)$
END PLOT
