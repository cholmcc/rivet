BEGIN PLOT /HRS_1987_I215848/d02-x01-y01
Title=$\pi^\pm$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}x_E$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /HRS_1987_I215848/d03-x01-y01
Title=$K^\pm$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}x_E$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /HRS_1987_I215848/d04-x01-y01
Title=$p,\bar{p}$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}x_E$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /HRS_1987_I215848/d05-x01-y01
Title=$K^0$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}x_E$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /HRS_1987_I215848/d06-x01-y01
Title=$\Lambda^0$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}x_E$ [$\mu\mathrm{b} \mathrm{GeV}^2$]
END PLOT
