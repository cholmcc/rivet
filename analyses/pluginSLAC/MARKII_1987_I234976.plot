BEGIN PLOT /MARKII_1987_I234976/d01-x01-y01
Title=$\Xi^-$ spectrum at 29 GeV
XLabel=$x_E$
YLabel=$\frac{s}{\beta} \mathrm{d}\sigma/\mathrm{d}x_E$ [$\mathrm{nb} \mathrm{GeV}^2$]
END PLOT
BEGIN PLOT /MARKII_1987_I234976/d02-x01-y01
Title=$\Xi^-$ cross section at 29 GeV
XLabel=$\sqrt{s}$
YLabel=$\sigma(\Xi^-)$
END PLOT
BEGIN PLOT /MARKII_1987_I234976/d02-x01-y02
Title=$\Xi^-$ rate at 29 GeV
XLabel=$\sqrt{s}$
YLabel=$N(\Xi^-)$
END PLOT
