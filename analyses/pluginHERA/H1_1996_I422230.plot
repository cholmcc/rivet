BEGIN PLOT /H1_1996_I422230/d0[1234]-.*
XLabel=$n$
YLabel=$P_n[\%]$
END PLOT


BEGIN PLOT /H1_1996_I422230/d01-x01-y01
Title=$P_n$ vs multiplicity $1 < \eta <2 $, $80 < W <115$ GeV
LegendXPos=0.45
END PLOT

BEGIN PLOT /H1_1996_I422230/d01-x01-y02
Title=$P_n$ vs multiplicity $1 < \eta < 3 $, $80 < W < 115 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d01-x01-y03
Title=$P_n$ vs multiplicity $1 < \eta < 4 $, $80 < W < 115 $ GeV
LegendYPos=0.6
LegendXPos=0.2
END PLOT

BEGIN PLOT /H1_1996_I422230/d01-x01-y04
Title=$P_n$ vs multiplicity $1 < \eta < 5 $, $80 < W < 115 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d02-x01-y01
Title=$P_n$ vs multiplicity $1 < \eta < 2 $, $115 < W < 150 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d02-x01-y02
Title=$P_n$ vs multiplicity $1 < \eta < 3 $, $115 < W < 150 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d02-x01-y03
Title=$P_n$ vs multiplicity $1 < \eta < 4 $, $115 < W < 150 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d02-x01-y04
Title=$P_n$ vs multiplicity $1 < \eta < 5 $, $115 < W < 150 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d03-x01-y01
Title=$P_n$ vs multiplicity $1 < \eta < 2 $, $150 < W < 185 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d03-x01-y02
Title=$P_n$ vs multiplicity $1 < \eta < 3 $, $150 < W < 185 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d03-x01-y03
Title=$P_n$ vs multiplicity $1 < \eta < 4 $, $150 < W < 185 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d03-x01-y04
Title=$P_n$ vs multiplicity $1 < \eta < 5 $, $150 < W < 185 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d04-x01-y01
Title=$P_n$ vs multiplicity $1 < \eta < 2 $, $185 < W < 220 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d04-x01-y02
Title=$P_n$ vs multiplicity $1 < \eta < 3 $ , $185 < W < 220 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d04-x01-y03
Title=$P_n$ vs multiplicity $1 < \eta < 4 $, $185 < W < 220 $ GeV
END PLOT

BEGIN PLOT /H1_1996_I422230/d04-x01-y04
Title=$P_n$ vs multiplicity $1 < \eta < 5 $, $185 < W < 220 $ GeV
END PLOT



BEGIN PLOT /H1_1996_I422230/d(05,06,07,08,09,10,11,12)-x01-y..
XLabel=$W$ [GeV]
END PLOT

BEGIN PLOT /H1_1996_I422230/d(05,06,07,08,09,10,11,12)-x01-y01
LogY=0
YLabel=$\langle n \rangle$
END PLOT

BEGIN PLOT /H1_1996_I422230/d(05,06,07,08,09,10,11,12)-x01-y02
YLabel=$D_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d(05,06,07,08,09,10,11,12)-x01-y03
YLabel=$D_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d(05,06,07,08,09,10,11,12)-x01-y04
YLabel=$D_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d(05,06,07,08,09,10,11,12)-x01-y05
YLabel=$C_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d(05,06,07,08,09,10,11,12)-x01-y06
YLabel=$C_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d(05,06,07,08,09,10,11,12)-x01-y07
YLabel=$C_4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d(05,06,07,08,09,10,11,12)-x01-y08
LogY=0
YMin=0
YMax=2.00
YLabel=$R_2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d(05,06,07,08,09,10,11,12)-x01-y09
LogY=0
YMin=0
YMax=2.00
YLabel=$R_3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d(05,06,07,08,09,10,11,12)-x01-y10
LogY=0
YMax=1.00
YLabel=$K_3$
END PLOT


BEGIN PLOT /H1_1996_I422230/d05-x01-y01
Title=Mean multiplicity vs $W$,   $ 0 < \eta$
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y02
Title=$D_2$ vs $W$,   $0< \eta$
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y03
Title=$D_3$ vs $W$,   $0 < \eta $
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y04
Title=$D_4$ vs $W$,   $0 < \eta $
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y05
Title=$C_2$ vs $W$,   $0 < \eta $
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y06
Title=$C_3$ vs $W$,   $0 < \eta $
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y07
Title=$C_4$ vs $W$,   $0 < \eta $
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y08
Title=$R_2$ vs $W$,   $0 < \eta $
END PLOT

BEGIN PLOT /H1_1996_I422230/d05-x01-y09
Title=$R_3$ vs $W$,   $0 < \eta $
END PLOT


BEGIN PLOT /H1_1996_I422230/d06-x01-y01
Title=Mean multiplicity vs $W$,   $1<\eta<2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y02
Title=$D_2$ vs $W$,   $1<\eta<2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y03
Title=$D_3$ vs $W$,   $1<\eta<2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y04
Title=$D_4$ vs $W$,   $1<\eta<2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y05
Title=$C_2$ vs $W$,   $1<\eta<2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y06
Title=$C_3$ vs $W$,   $1<\eta<2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y07
Title=$C_4$ vs $W$,   $1<\eta<2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y08
YMin=0.5
Title=$R_2$ vs $W$,   $1<\eta<2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y09
YMin=1
Title=$R_3$ vs $W$,   $1<\eta<2$
END PLOT

BEGIN PLOT /H1_1996_I422230/d06-x01-y10
YMin=0.0001
Title=$K_3$ vs $W$,   $1<\eta<2$
END PLOT


BEGIN PLOT /H1_1996_I422230/d07-x01-y01
Title=Mean multiplicity vs $W$,   $1<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y02
Title=$D_2$ vs $W$,   $1<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y03
Title=$D_3$ vs $W$,   $1<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y04
Title=$D_4$ vs $W$,   $1<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y05
Title=$C_2$ vs $W$,   $1<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y06
Title=$C_3$ vs $W$,   $1<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y07
Title=$C_4$ vs $W$,   $1<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y08
YMin=0.5
Title=$R_2$ vs $W$,   $1<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y09
YMin=0.5
Title=$R_3$ vs $W$,   $1<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d07-x01-y10
YMin=0.0001
Title=$K_3$ vs $W$,   $1<\eta<3$
END PLOT


BEGIN PLOT /H1_1996_I422230/d08-x01-y01
Title=Mean multiplicity vs $W$,   $1<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y02
Title=$D_2$ vs $W$,   $1<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y03
Title=$D_3$ vs $W$,   $1<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y04
Title=$D_4$ vs $W$,   $1<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y05
Title=$C_2$ vs $W$,   $1<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y06
Title=$C_3$ vs $W$,   $1<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y07
Title=$C_4$ vs $W$,   $1<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y08
Title=$R_2$ vs $W$,   $1<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d08-x01-y09
Title=$R_3$ vs $W$,   $1<\eta<4$
END PLOT


BEGIN PLOT /H1_1996_I422230/d09-x01-y01
Title=Mean multiplicity vs $W$,   $1<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y02
Title=$D_2$ vs $W$,   $1<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y03
Title=$D_3$ vs $W$,   $1<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y04
Title=$D_4$ vs $W$,   $1<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y05
Title=$C_2$ vs $W$,   $1<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y06
Title=$C_3$ vs $W$,   $1<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y07
Title=$C_4$ vs $W$,   $1<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y08
Title=$R_2$ vs $W$,   $1<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d09-x01-y09
Title=$R_3$ vs $W$,   $1<\eta<5$
END PLOT


BEGIN PLOT /H1_1996_I422230/d10-x01-y01
Title=Mean multiplicity vs $W$,   $2<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y02
Title=$D_2$ vs $W$,   $2<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y03
Title=$D_3$ vs $W$,   $2<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y04
Title=$D_4$ vs $W$,   $2<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y05
Title=$C_2$ vs $W$,   $2<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y06
Title=$C_3$ vs $W$,   $2<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y07
Title=$C_4$ vs $W$,   $2<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y08
Title=$R_2$ vs $W$,   $2<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y09
YMin=1
YMax=3.5
Title=$R_3$ vs $W$,   $2<\eta<3$
END PLOT

BEGIN PLOT /H1_1996_I422230/d10-x01-y10
YMin=-0.4
YMax=0.6
Title=$K_3$ vs $W$,   $2<\eta<3$
END PLOT


BEGIN PLOT /H1_1996_I422230/d11-x01-y01
Title=Mean multiplicity vs $W$,   $3<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y02
Title=$D_2$ vs $W$,   $3<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y03
Title=$D_3$ vs $W$,   $3<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y04
Title=$D_4$ vs $W$,   $3<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y05
Title=$C_2$ vs $W$,   $3<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y06
Title=$C_3$ vs $W$,   $3<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y07
Title=$C_4$ vs $W$,   $3<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y08
Title=$R_2$ vs $W$,   $3<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y09
Title=$R_3$ vs $W$,   $3<\eta<4$
END PLOT

BEGIN PLOT /H1_1996_I422230/d11-x01-y10
Title=$K_3$ vs $W$,   $3<\eta<4$
END PLOT


BEGIN PLOT /H1_1996_I422230/d12-x01-y01
Title=Mean multiplicity vs $W$,   $4<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y02
Title=$D_2$ vs $W$,   $4<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y03
Title=$D_3$ vs $W$,   $4<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y04
Title=$D_4$ vs $W$,   $4<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y05
Title=$C_2$ vs $W$,   $4<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y06
Title=$C_3$ vs $W$,   $4<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y07
Title=$C_4$ vs $W$,   $4<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y08
Title=$R_2$ vs $W$,   $4<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y09
Title=$R_3$ vs $W$,   $4<\eta<5$
END PLOT

BEGIN PLOT /H1_1996_I422230/d12-x01-y10
YMin=-0.15
Title=$K_3$ vs $W$, ,   $4<\eta<5$
END PLOT
