

# ... add more histograms as you need them ...
BEGIN PLOT /H1_1999_I481112/d02-x01-y01

Title=Transverse D* momentum lab frame

XLabel=$p_T$ [GeV]
YLabel=$d\sigma/dp_T$ [$nb GeV^{-1}$]
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_1999_I481112/d03-x01-y01

Title= Transverse D* momentum HCM
XLabel=$p_T *$ [GeV]
YLabel=$d\sigma/dp_T*$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_1999_I481112/d04-x01-y01

Title= D* pseudo-rapidity
XLabel=$\eta$  
YLabel=$d\sigma/d\eta$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_1999_I481112/d05-x01-y01

Title=four-momentum transfer squared
XLabel=$Q^2$ [$GeV^2$]
YLabel=$d\sigma/dQ^2$
LogX=1
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_1999_I481112/d06-x01-y01

Title=DIS
XLabel=$\log(x_{g OBS})$  
YLabel=$d\sigma/d\log(x_g OBS)$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_1999_I481112/d07-x01-y01

Title=Differential photoproduction cross section W= 194 GeV
XLabel=$y$ 
YLabel=$d\sigma_{\gamma p}/dy$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_1999_I481112/d08-x01-y01

Title=Differential photoproduction cross section W= 194 GeV
XLabel=$p_T$ [GeV]
YLabel=$d\sigma_{\gamma p}/dp_T$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_1999_I481112/d09-x01-y01

Title= Differential photoproduction cross section W=88 GeV
XLabel=$y$
YLabel=$d\sigma_{\gamma p}/dy$
# + any additional plot settings you might like, see make-plots documentation
END PLOT

BEGIN PLOT /H1_1999_I481112/d10-x01-y01

Title=Differential photoproduction cross section W= 88 GeV
XLabel=$p_T$ [GeV]
YLabel=$d\sigma_{\gamma p}/dp_T$
# + any additional plot settings you might like, see make-plots documentation
END PLOT


BEGIN PLOT /H1_1999_I481112/d11-x01-y01

Title=Double-differential cross section $2.5 GeV < p_T < 3.5 GeV$
XLabel=$y$ [GeV]
YLabel=$d\sigma_{\gamma p}/dy$
# + any additional plot settings you might like, see make-plots documentation
END PLOT



BEGIN PLOT /H1_1999_I481112/d11-x01-y02

Title=Double-differential cross section $3.5 GeV < p_T < 5.0 GeV$
XLabel=$y$ [GeV]
YLabel=$d\sigma_{\gamma p}/dy$
# + any additional plot settings you might like, see make-plots documentation
END PLOT


BEGIN PLOT /H1_1999_I481112/d12-x01-y01

Title=Differential cross section photoproduction $W = 223 GeV$
XLabel=$\log(x_g OBS)$ [GeV]
YLabel=$d\sigma_{\gamma p}/d\log(x_{g OBS})$
# + any additional plot settings you might like, see make-plots documentation
END PLOT


BEGIN PLOT /H1_1999_I481112/d12-x01-y02

Title=Differential cross section photoproduction $W = 185GeV$
XLabel=$\log(x_g OBS)$ [GeV]
YLabel=$d\sigma_{\gamma p}/d\log(x_{g OBS})$
END PLOT


BEGIN PLOT /H1_1999_I481112/d13-x01-y01

Title=Differential cross section photoproduction $W = 88 GeV$
XLabel=$\log(x_g OBS)$ [GeV]
YLabel=$d\sigma_{\gamma p}/d\log(x_{g OBS})$
END PLOT


BEGIN PLOT /H1_1999_I481112/d11-x01-y03
Title=Double-differential cross section $5.0 GeV < p_T < 10.5 GeV$
XLabel=$y$ [GeV]
YLabel=$d\sigma_{\gamma p}/dy$
# + any additional plot settings you might like, see make-plots documentation
END PLOT









