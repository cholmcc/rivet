#BEGIN PLOT /ALICE_2021_I1891391/d02-x01-y01
LogY=0
XLabel=$\Delta\varphi$ (rad)
YLabel=$1/N_{\rm{trigg}}\tex{d}N/\tex{d}\Delta\varphi$ $(\rm{rad}^{-1})$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d03-x01-y01
LogY=0
XLabel=$\Delta\varphi$ (rad)
YLabel=$1/N_{\rm{trigg}}\tex{d}N/\tex{d}\Delta\varphi$ $(\rm{rad}^{-1})$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d04-x01-y01
LogY=0
XLabel=$\Delta\varphi$ (rad)
YLabel=$1/N_{\rm{trigg}}\tex{d}N/\tex{d}\Delta\varphi$ $(\rm{rad}^{-1})$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d05-x01-y01
LogY=0
FullRange=1
XLabel=$\Delta\varphi$ (rad)
YLabel=$1/N_{\rm{trigg}}\tex{d}N/\tex{d}\Delta\varphi$ $(\rm{rad}^{-1})$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d06-x01-y01
LogY=0
FullRange=1
XLabel=$\Delta\varphi$ (rad)
YLabel=$1/N_{\rm{trigg}}\tex{d}N/\tex{d}\Delta\varphi$ $(\rm{rad}^{-1})$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d07-x01-y01
LogY=0
FullRange=1
XLabel=$\Delta\varphi$ (rad)
YLabel=$1/N_{\rm{trigg}}\tex{d}N/\tex{d}\Delta\varphi$ $(\rm{rad}^{-1})$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d08-x01-y01
LogY=0
XMin=2.5
XMax=20.5
YMin=0.5
YMax=6.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d08-x01-y02
LogY=0
XMin=2.5
XMax=20.5
YMin=0.5
YMax=6.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d08-x01-y03
LogY=0
XMin=2.5
XMax=20.5
YMin=0.5
YMax=6.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d08-x01-y04
LogY=0
XMin=2.5
XMax=20.5
YMin=0.5
YMax=6.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d08-x01-y05
LogY=0
XMin=2.5
XMax=20.5
YMin=0.5
YMax=6.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d08-x01-y06
LogY=0
XMin=2.5
XMax=20.5
YMin=0.5
YMax=6.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d08-x01-y07
LogY=0
XMin=2.5
XMax=20.5
YMin=0.5
YMax=6.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d09-x01-y01
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=4.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d09-x01-y02
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=4.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d09-x01-y03
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=4.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d09-x01-y04
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=4.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d09-x01-y05
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=4.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d09-x01-y06
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=4.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d09-x01-y07
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=4.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d10-x01-y01
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=9.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d10-x01-y02
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=9.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d10-x01-y03
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=9.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d10-x01-y04
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=9.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d10-x01-y05
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=9.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d10-x01-y06
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=9.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d10-x01-y07
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.5
YMax=9.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d11-x01-y01
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=3.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d11-x01-y02
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=3.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d11-x01-y03
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=3.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d11-x01-y04
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=3.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d11-x01-y05
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=3.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d11-x01-y06
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=3.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d11-x01-y07
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=3.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d12-x01-y01
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=4
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d12-x01-y02
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=4
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d12-x01-y03
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=4
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d12-x01-y04
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=4
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d12-x01-y05
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=4
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d12-x01-y06
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=4
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d12-x01-y07
LogY=0
FullRange=1
XMin=2.5
XMax=20.5
YMin=0.3
YMax=4
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d13-x01-y01
LogY=0
XMin=2.5
XMax=20.5
YMin=0.3
YMax=10
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d13-x01-y02
LogY=0
XMin=2.5
XMax=20.5
YMin=0.3
YMax=10
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d13-x01-y03
LogY=0
XMin=2.5
XMax=20.5
YMin=0.3
YMax=10
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d13-x01-y04
LogY=0
XMin=2.5
XMax=20.5
YMin=0.3
YMax=10
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d13-x01-y05
LogY=0
XMin=2.5
XMax=20.5
YMin=0.3
YMax=10
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d13-x01-y06
LogY=0
XMin=2.5
XMax=20.5
YMin=0.3
YMax=10
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d13-x01-y07
LogY=0
XMin=2.5
XMax=20.5
YMin=0.3
YMax=10
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d14-x01-y01
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{0-1\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d14-x01-y02
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{1-3\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d14-x01-y03
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{3-7\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d14-x01-y04
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{7-15\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d14-x01-y05
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{15-50\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d14-x01-y06
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{50-100\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT


BEGIN PLOT /ALICE_2021_I1891391/d15-x01-y01
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{0-1\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d15-x01-y02
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{1-3\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d15-x01-y03
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{3-7\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d15-x01-y04
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{7-15\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d15-x01-y05
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{15-50\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d15-x01-y06
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{50-100\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d16-x01-y01
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{0-1\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d16-x01-y02
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{1-3\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d16-x01-y03
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{3-7\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d16-x01-y04
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{7-15\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d16-x01-y05
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{15-50\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d16-x01-y06
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{50-100\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d17-x01-y01
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{0-1\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d17-x01-y02
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{1-3\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d17-x01-y03
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{3-7\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d17-x01-y04
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{7-15\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d17-x01-y05
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{15-50\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d17-x01-y06
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}_{50-100\%}$/$Y_{\Delta\varphi}^{h-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d18-x01-y01
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{0-1\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d18-x01-y02
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{1-3\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d18-x01-y03
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{3-7\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d18-x01-y04
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{7-15\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d18-x01-y05
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{15-50\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d18-x01-y06
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}_{50-100\%}$/$Y_{\Delta\varphi}^{K_S^0-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d19-x01-y01
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{0-1\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d19-x01-y02
LogY=0
XMin=2.5
XMax=11.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{1-3\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d19-x01-y03
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{3-7\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d19-x01-y04
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{7-15\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d19-x01-y05
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{15-50\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d19-x01-y06
LogY=0
XMin=2.5
XMax=20.5
YMin=0
YMax=2
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{50-100\%}$/$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}_{MB}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d20-x01-y01
XMin=0.8
XMax=3.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d20-x01-y02
XMin=0.8
XMax=4.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d20-x01-y03
XMin=0.8
XMax=5.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d20-x01-y04
XMin=0.8
XMax=6.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d20-x01-y05
XMin=0.5
XMax=7.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d20-x01-y06
XMin=0.5
XMax=9.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d20-x01-y07
XMin=0.5
XMax=11.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d20-x01-y08
XMin=0.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d21-x01-y01
XMin=0.8
XMax=3.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d21-x01-y02
XMin=0.8
XMax=4.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d21-x01-y03
XMin=0.8
XMax=5.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d21-x01-y04
XMin=0.8
XMax=6.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d21-x01-y05
XMin=0.5
XMax=7.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d21-x01-y06
XMin=0.5
XMax=9.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d21-x01-y07
XMin=0.5
XMax=11.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d21-x01-y08
XMin=0.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d22-x01-y01
XMin=0.8
XMax=3.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}))-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d22-x01-y02
XMin=0.8
XMax=4.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}))-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d22-x01-y03
XMin=0.8
XMax=5.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}))-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d22-x01-y04
XMin=0.8
XMax=6.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}))-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d22-x01-y05
XMin=0.5
XMax=7.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}))-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d22-x01-y06
XMin=0.5
XMax=9.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}))-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d22-x01-y07
XMin=0.5
XMax=11.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda}))-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d23-x01-y01
XMin=0.8
XMax=3.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d23-x01-y02
XMin=0.8
XMax=4.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d23-x01-y03
XMin=0.8
XMax=5.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d23-x01-y04
XMin=0.8
XMax=6.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d23-x01-y05
XMin=0.5
XMax=7.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d23-x01-y06
XMin=0.5
XMax=9.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d23-x01-y07
XMin=0.5
XMax=11.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d23-x01-y08
XMin=0.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{h-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d24-x01-y01
XMin=0.8
XMax=3.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d24-x01-y02
XMin=0.8
XMax=4.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d24-x01-y03
XMin=0.8
XMax=5.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d24-x01-y04
XMin=0.8
XMax=6.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d24-x01-y05
XMin=0.5
XMax=7.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d24-x01-y06
XMin=0.5
XMax=9.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d24-x01-y07
XMin=0.5
XMax=11.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d24-x01-y08
XMin=0.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{hK_S^0-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d25-x01-y01
XMin=0.8
XMax=3.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d25-x01-y02
XMin=0.8
XMax=4.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{K(\Lambda+\overline{\Lambda})-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d25-x01-y03
XMin=0.8
XMax=5.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d25-x01-y04
XMin=0.8
XMax=6.2
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d25-x01-y05
XMin=0.5
XMax=7.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d25-x01-y06
XMin=0.5
XMax=9.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

BEGIN PLOT /ALICE_2021_I1891391/d25-x01-y07
XMin=0.5
XMax=11.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{(\Lambda+\overline{\Lambda})-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d26-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d27-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d28-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d29-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d30-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d31-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d32-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d33-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d34-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d35-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d36-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d37-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d38-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d39-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d40-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d41-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d42-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d43-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d44-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d45-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d46-x01-y01
LogY=0
XMin=2.5
XMax=15.5
FullRange=1
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d47-x01-y01
LogY=0
XMin=2.5
XMax=15.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d48-x01-y01
LogY=0
XMin=2.5
XMax=15.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d49-x01-y01
LogY=0
XMin=2.5
XMax=15.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d50-x01-y01
LogY=0
XMin=2.5
XMax=15.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d51-x01-y01
LogY=0
XMin=2.5
XMax=15.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d52-x01-y01
LogY=0
XMin=2.5
XMax=15.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d53-x01-y01
LogY=0
XMin=2.5
XMax=15.5
XLabel=$p_{\rm T}^{\rm {trigg}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d54-x01-y01
LogY=0
XMin=0.8
XMax=3.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d55-x01-y01
LogY=0
XMin=0.8
XMax=4.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d56-x01-y01
LogY=0
XMin=0.8
XMax=5.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d57-x01-y01
LogY=0
XMin=0.8
XMax=6.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d58-x01-y01
LogY=0
XMin=0.5
XMax=7.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d59-x01-y01
LogY=0
XMin=0.5
XMax=9.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d60-x01-y01
LogY=0
XMin=0.5
XMax=11.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d61-x01-y01
LogY=0
XMin=0.8
XMax=3.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d62-x01-y01
LogY=0
XMin=0.8
XMax=4.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d63-x01-y01
LogY=0
XMin=0.8
XMax=5.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d64-x01-y01
LogY=0
XMin=0.8
XMax=6.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d65-x01-y01
LogY=0
XMin=0.5
XMax=7.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d66-x01-y01
LogY=0
XMin=0.5
XMax=9.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d67-x01-y01
LogY=0
XMin=0.5
XMax=11.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d68-x01-y01
LogY=0
XMin=0.8
XMax=3.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d69-x01-y01
LogY=0
XMin=0.8
XMax=4.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d70-x01-y01
LogY=0
XMin=0.8
XMax=5.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d71-x01-y01
LogY=0
XMin=0.8
XMax=6.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d72-x01-y01
LogY=0
XMin=0.5
XMax=7.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d73-x01-y01
LogY=0
XMin=0.5
XMax=9.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d74-x01-y01
LogY=0
XMin=0.5
XMax=11.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^(K_S^0-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d75-x01-y01
LogY=0
XMin=0.8
XMax=3.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d76-x01-y01
LogY=0
XMin=0.8
XMax=4.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d77-x01-y01
LogY=0
XMin=0.8
XMax=5.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d78-x01-y01
LogY=0
XMin=0.8
XMax=6.2
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d79-x01-y01
LogY=0
XMin=0.5
XMax=7.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d80-x01-y01
LogY=0
XMin=0.5
XMax=9.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

#BEGIN PLOT /ALICE_2021_I1891391/d81-x01-y01
LogY=0
XMin=0.5
XMax=6.5
XLabel=$p_{\rm T}^{\rm {assoc}}$(GeV/$c$)
YLabel=$Y_{\Delta\varphi}^{Y_{\Delta\varphi}^((\Lambda+\overline{\Lambda})-h}/Y_{\Delta\varphi}^{h-h}$
#END PLOT

# ... add more histograms as you need them ...
