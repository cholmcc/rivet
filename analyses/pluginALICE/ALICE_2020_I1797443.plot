BEGIN PLOT /ALICE_2020_I1797443/d01-x01-y01
Title=ALICE, 13 TeV, $\pi^{\pm}$
XLabel=$p_{T}$ [GeV]
YLabel=$1/N \times \mathrm{d}^2/(\mathrm{d}p_T \mathrm{d}y)$ [1/GeV]
LogY=1
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d02-x01-y01
Title=ALICE, 13 TeV, $K^{\pm}$
XLabel=$p_{T}$ [GeV]
YLabel=$1/N \times \mathrm{d}^2/(\mathrm{d}p_T \mathrm{d}y)$ [1/GeV]
LogY=1
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d03-x01-y01
Title=ALICE, 13 TeV, $K^0_S$
XLabel=$p_{T}$ [GeV]
YLabel=$1/N \times \mathrm{d}^2/(\mathrm{d}p_T \mathrm{d}y)$ [1/GeV]
LogY=1
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d04-x01-y01
Title=ALICE, 13 TeV, $K^{*0} + \overline{K}^{*0}$
XLabel=$p_{T}$ [GeV]
YLabel=$1/N \times \mathrm{d}^2/(\mathrm{d}p_T \mathrm{d}y)$ [1/GeV]
LogY=1
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d05-x01-y01
Title=ALICE, 13 TeV, $\phi$
XLabel=$p_{T}$ [GeV]
YLabel=$1/N \times \mathrm{d}^2/(\mathrm{d}p_T \mathrm{d}y)$ [1/GeV]
LogY=1
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d06-x01-y01
Title=ALICE, 13 TeV, $p + \overline{p}$
XLabel=$p_{T}$ [GeV]
YLabel=$1/N \times \mathrm{d}^2/(\mathrm{d}p_T \mathrm{d}y)$ [1/GeV]
LogY=1
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d07-x01-y01
Title=ALICE, 13 TeV, $\Lambda + \overline{\Lambda}$
XLabel=$p_{T}$ [GeV]
YLabel=$1/N \times \mathrm{d}^2/(\mathrm{d}p_T \mathrm{d}y)$ [1/GeV]
LogY=1
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d08-x01-y01
Title=ALICE, 13 TeV, $\Xi^- + \overline{\Xi}^+$
XLabel=$p_{T}$ [GeV]
YLabel=$1/N \times \mathrm{d}^2/(\mathrm{d}p_T \mathrm{d}y)$ [1/GeV]
LogY=1
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d09-x01-y01
Title=ALICE, 13 TeV, $\Omega^- + \overline{\Omega}^+$
XLabel=$p_{T}$ [GeV]
YLabel=$1/N \times \mathrm{d}^2/(\mathrm{d}p_T \mathrm{d}y)$ [1/GeV]
LogY=1
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d21-x01-y01
Title=ALICE, 13 TeV
XLabel=$p_{T}$ [GeV]
YLabel=$(K^+ + K^-) / (\pi^+ + \pi^-)$
YMin=0.0
YMax=1.0
LogY=0
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d22-x01-y01
Title=ALICE, 13 TeV
XLabel=$p_{T}$ [GeV]
YLabel=$(K^{*0} + \overline{K}^{*0}) / (\pi^+ + \pi^-)$
YMin=0.0
YMax=1.0
LogY=0
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d23-x01-y01
Title=ALICE, 13 TeV
XLabel=$p_{T}$ [GeV]
YLabel=$2 K^0_S / (\pi^+ + \pi^-)$
YMin=0.0
YMax=1.0
LogY=0
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d24-x01-y01
Title=ALICE, 13 TeV
XLabel=$p_{T}$ [GeV]
YLabel=$2 \phi / (\pi^+ + \pi^-)$
YMin=0.0
YMax=0.5
LogY=0
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d42-x01-y01
Title=ALICE, 13 TeV
XLabel=$p_{T}$ [GeV]
YLabel=$p / \pi$
YMin=0.0
YMax=0.5
LogY=0
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d43-x01-y01
Title=ALICE, 13 TeV
XLabel=$p_{T}$ [GeV]
YLabel=$\Lambda / 2 K^0_S$
YMin=0.0
YMax=1.0
LogY=0
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d44-x01-y01
Title=ALICE, 13 TeV
XLabel=$p_{T}$ [GeV]
YLabel=$\Omega / \phi$
YMin=0.0
YMax=0.2
LogY=0
END PLOT

BEGIN PLOT /ALICE_2020_I1797443/d45-x01-y01
Title=ALICE, 13 TeV
XLabel=$p_{T}$ [GeV]
YLabel=$\Xi / \phi$
YMin=0.0
YMax=1.0
LogY=0
END PLOT